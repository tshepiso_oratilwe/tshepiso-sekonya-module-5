
class Todo {
  String title;
  String id;
  String description;
  bool isComplete;

  Todo({
    required this.title,
    this.description = '',
    this.id = '',
    this.isComplete = false,
  });

  // Returns json version of Todo objec
  Map<String, dynamic> toJson() => ({
        'title': title,
        'description': description,
        'isComplete': isComplete,
        'id': id,
      });

  // Returns Todo object from given json
  static Todo fromJson(Map<String, dynamic> json) => Todo(
        title: json['title'],
        id: json['id'],
        description: json['description'],
        isComplete: json['isComplete'],
      );
}
