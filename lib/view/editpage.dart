import 'package:flutter/material.dart';
import 'package:todo_app/api/firebase_api.dart';

class EditPage extends StatefulWidget {
  // ignore: prefer_typing_uninitialized_variables
  final todo;
  // const EditPage({Key? key}) : super(key: key);
  // ignore: use_key_in_widget_constructors
  const EditPage({required this.todo});

  @override
  State<EditPage> createState() => _EditPageState();
}

class _EditPageState extends State<EditPage> {
  // ignore: non_constant_identifier_names
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[50],
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 36,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.close,
                          size: 16,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                    icon: Container(
                      width: 20,
                      height: 20,
                      decoration: BoxDecoration(
                        color: Colors.blue[200],
                        borderRadius: BorderRadius.circular(4),
                      ),
                    ),
                    hintText: 'Title',
                    border: InputBorder.none,
                  ),
                  initialValue: "${widget.todo.title}",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: Colors.grey[600],
                  ),
                  onChanged: (value) {
                    setState(() {
                      widget.todo.title = value;
                    });
                  },
                ),
                const SizedBox(height: 36),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: const Color(0xFFFAF6EA),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: 8,
                    ),
                    child: TextFormField(
                      minLines: 5,
                      maxLines: 5,
                      keyboardType: TextInputType.multiline,
                      initialValue: "${widget.todo.description}",
                      style: const TextStyle(color: Colors.amber),
                      decoration: const InputDecoration(
                        hintText: 'Write a note...',
                        hintStyle: TextStyle(
                          color: Colors.amber,
                        ),
                        border: InputBorder.none,
                      ),
                      onChanged: (value) {
                        setState(() {
                          widget.todo.description = value;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.blue[50],
                      primary: Colors.blueGrey,
                    ),
                    onPressed: () {
                      FirebaseApi.updateTodo(widget.todo);
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: const [
                        Icon(
                          Icons.check,
                          size: 20,
                        ),
                        SizedBox(width: 8),
                        Text("Save"),
                      ],
                    ),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Colors.red[50],
                      primary: Colors.red,
                    ),
                    onPressed: () {
                      FirebaseApi.deleteTodo(widget.todo);
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: const [
                        Icon(
                          Icons.delete,
                          size: 20,
                        ),
                        SizedBox(width: 8),
                        Text("Delete"),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
